# Configure the Azure provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.98.0"
    }
  }

  required_version = ">= 1.1.0"
}

provider "azurerm" {
  features {}

  subscription_id = "298ede54-46be-432e-8e63-31b0f5969505"
  tenant_id       = "190ce420-b157-44ae-bc2f-69563baa5a3b"
}

resource "azurerm_resource_group" "Linux" {
  name     = "Linux-resources"
  location = "eastus"
}

resource "azurerm_public_ip" "Linux" {
  name                = "Linux-PublicIp"
  resource_group_name = azurerm_resource_group.Linux.name
  location            = azurerm_resource_group.Linux.location
  allocation_method   = "Static"

  provisioner "local-exec" {
    command = "rm -f inventory && cp template/inventory ./ && echo ${self.ip_address} >> inventory"
  }
}


resource "azurerm_virtual_network" "Linux" {
  name                = "Linux-network"
  address_space       = ["10.0.0.0/16"]
  location            = "eastus"
  resource_group_name = azurerm_resource_group.Linux.name
}

resource "azurerm_subnet" "internal" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.Linux.name
  virtual_network_name = azurerm_virtual_network.Linux.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_interface" "Linux" {
  name                = "Linux-nic"
  resource_group_name = azurerm_resource_group.Linux.name
  location            = "eastus"

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.internal.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.Linux.id
  }
}

resource "azurerm_linux_virtual_machine" "Linux" {
  name                            = "Linux-vm"
  resource_group_name             = azurerm_resource_group.Linux.name
  location                        = "eastus"
  size                            = "Standard_B1ms"
  admin_username                  = "adminuser"
  custom_data                     = base64encode("Hello World!")
  disable_password_authentication = true
  source_image_id		  = var.azimg_id 
  network_interface_ids = [
    azurerm_network_interface.Linux.id,
  ]

  tags = {
    Scope = "VM"
    Env   = "Linux"

  }

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }

  admin_ssh_key {
    username   = "adminuser"
    public_key = file("/home/user/.ssh/id_rsa.pub")
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u adminuser -i inventory ansible/wp_config.yml"
  }

}

output "public_adresses_linux" {
  value = azurerm_public_ip.Linux.ip_address
  description = "Affichage de l'adresse ip publique du serveur"
}
