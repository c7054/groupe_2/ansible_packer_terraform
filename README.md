# DEPLOIEMENT AUTOMATISE D'INSTANCES VIA L'INTÉGRATION D'ANSIBLE, PACKER ET TERRAFORM

L'intégration des trois outils dans un même projet permet d'automatiser les processus de déploiement d'instances.

Rôles des outils : 
 - Ansible - Gérer l'installation de paquets, services, utilisateurs, ... sur une machine.
 - Packer - Créer une image de déploiement customisée.
 - Terraform - Créer une instance personnalisé sur différentes plateformes (Azure, AWS, ...).

## Arborescence du projet
```
.
├── ansible
│   ├── README.md
│   ├── roles
│   │   ├── apache
│   │   │   ├── defaults
│   │   │   │   └── main.yml
│   │   │   ├── handlers
│   │   │   │   └── main.yml
│   │   │   ├── README.md
│   │   │   ├── tasks
│   │   │   │   └── main.yml
│   │   │   ├── tests
│   │   │   │   ├── inventory
│   │   │   │   └── test.yml
│   │   │   └── vars
│   │   │       └── main.yml
│   │   ├── common
│   │   │   ├── defaults
│   │   │   │   └── main.yml
│   │   │   ├── handlers
│   │   │   │   └── main.yml
│   │   │   ├── README.md
│   │   │   ├── tasks
│   │   │   │   └── main.yml
│   │   │   ├── tests
│   │   │   │   ├── inventory
│   │   │   │   └── test.yml
│   │   │   └── vars
│   │   │       └── main.yml
│   │   ├── mysql
│   │   │   ├── defaults
│   │   │   │   └── main.yml
│   │   │   ├── handlers
│   │   │   │   └── main.yml
│   │   │   ├── README.md
│   │   │   ├── tasks
│   │   │   │   └── main.yml
│   │   │   ├── tests
│   │   │   │   ├── inventory
│   │   │   │   └── test.yml
│   │   │   └── vars
│   │   │       └── main.yml
│   │   ├── php
│   │   │   ├── defaults
│   │   │   │   └── main.yml
│   │   │   ├── handlers
│   │   │   │   └── main.yml
│   │   │   ├── README.md
│   │   │   ├── tasks
│   │   │   │   └── main.yml
│   │   │   ├── tests
│   │   │   │   ├── inventory
│   │   │   │   └── test.yml
│   │   │   └── vars
│   │   │       └── main.yml
│   │   └── wordpress
│   │       ├── defaults
│   │       │   └── main.yml
│   │       ├── files
│   │       │   └── wordpress.conf
│   │       ├── handlers
│   │       │   └── main.yml
│   │       ├── README.md
│   │       ├── tasks
│   │       │   └── main.yml
│   │       ├── tests
│   │       │   ├── inventory
│   │       │   └── test.yml
│   │       └── vars
│   │           └── main.yml
│   └── wordpress.yml
├── azure-ubuntu.pkr.json.pkr.hcl
├── makefile
├── manifest.json
├── README.md
├── scripts
│   ├── deploy_ansible_debian.sh
│   ├── deploy_ansible_ubuntu.sh
│   ├── get_azimg_id.sh
│   ├── README.md
│   └── register_image_id.sh
└── terraform
    ├── ansible
    │   ├── roles
    │   │   ├── mysql
    │   │   │   ├── defaults
    │   │   │   │   └── main.yml
    │   │   │   ├── handlers
    │   │   │   │   └── main.yml
    │   │   │   ├── README.md
    │   │   │   ├── tasks
    │   │   │   │   └── main.yml
    │   │   │   ├── tests
    │   │   │   │   ├── inventory
    │   │   │   │   └── test.yml
    │   │   │   └── vars
    │   │   │       └── main.yml
    │   │   ├── sendmail
    │   │   │   ├── defaults
    │   │   │   │   └── main.yml
    │   │   │   ├── handlers
    │   │   │   │   └── main.yml
    │   │   │   ├── README.md
    │   │   │   ├── tasks
    │   │   │   │   └── main.yml
    │   │   │   ├── tests
    │   │   │   │   ├── inventory
    │   │   │   │   └── test.yml
    │   │   │   └── vars
    │   │   │       └── main.yml
    │   │   └── wordpress
    │   │       ├── defaults
    │   │       │   └── main.yml
    │   │       ├── files
    │   │       │   └── wordpress.conf
    │   │       ├── handlers
    │   │       │   └── main.yml
    │   │       ├── README.md
    │   │       ├── tasks
    │   │       │   └── main.yml
    │   │       ├── tests
    │   │       │   ├── inventory
    │   │       │   └── test.yml
    │   │       └── vars
    │   │           └── main.yml
    │   └── wp_config.yml
    ├── azimgvar.tf
    ├── inventory
    ├── main.tf
    ├── README.md
    ├── template
    │   └── inventory
    ├── terraform.tfstate
    └── terraform.tfstate.backup
```
