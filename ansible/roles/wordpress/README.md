Role Name
=========

wordpress

Requirements
------------

 - Fichier de conf apache

Role Variables
--------------

wordpress_directory_debian -> Répertoire d'installation de WordPress (ex: /var/www/wordpress)
wordpress_directory_user_debian: -> Utilisateur du répertoire (ex: www-data)

Dependencies
------------

Pas de dépendances.

Objectifs
----------------

 - Copier la conf d'apache sur le serveur distant
 - Desactiver la conf par défaut d'apache
 - Activer la conf apache copiée
 - Redemarrer le service apache
 - Créer répertoire WordPress
 - Télécharger wp-cli
 - Télécharger WordPress

Fichiers / Dossiers utilisés
------------------

tasks/main.yml
vars/main.yml
files/wordpress.conf
