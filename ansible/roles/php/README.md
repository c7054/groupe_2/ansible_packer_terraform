Role Name
=========

Php

Requirements
------------

Pas de pré-requis.

Role Variables
--------------

Pas de variables.

Dependencies
------------

Pas de dépendances.

Example Playbook
----------------

Installer les paquets php :
 - php
 - libapache2-mod-php
 - php-mysql
 - php-curl
 - php-gd 
 - php-mbstring
 - php-xml 
 - php-zip

Fichiers / Dossiers utilisés
------------------

tasks/main.yml
