Role Name
=========

Common

Requirements
------------

Pas de pré-requis.

Role Variables
--------------

Pas de varibales.

Dependencies
------------

Pas de dépendances.

Objectifs
----------------

Installer les paquets standard :
 - wget
 - vim
 - rsync
 - curl
 - python (Debian et Ubuntu)
 - python-pip (Debian et Ubuntu)
 - python-pymysql (Debian et Ubuntu)
 - python3 (CentOS)
 - python3-pip (CentOS)
 - python3-pymysql (CentOS)

Fichiers / Dossiers utilisés
----------------------------

tasks/main.yml
