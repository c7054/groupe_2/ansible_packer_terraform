Role Name
=========

Apache

Requirements
------------

Pas de pré-requis.

Role Variables
--------------

Pas de variables.

Dependencies
------------

Pas de dépendances.

Objectifs
----------------

Installer, démarrer et activer au démarrage le service apache2.

Fichiers / Dossiers utilisés
------------------

tasks/main.yml
