Role Name
=========

Mysql

Requirements
------------

Pas de pré-requis.

Role Variables
--------------

Pas de variables.

Dependencies
------------

Pas de dépendances.

Objectifs
----------------

Installer, démarrer et activer au démarrage le service mysql (Ubuntu) / mariadb (Debian).

Fichiers / Dossiers utilisés
------------------

tasks/main.yml
