# DEPLOIEMENT AUTOMATISE D'INSTANCES VIA L'INTÉGRATION D'ANSIBLE, PACKER ET TERRAFORM

Ce dossier contient les différents éléments suivants : 
 - Un fichier d'appel global des rôles ansible
 - Un ensemble de répertoire appelé "rôles" qui contiennent la configuration de différents services.

Objectifs :
 - wordpress.yml -> Fichier appelé par Packer qui indique les différents rôles à executer.
 - roles/common/ -> Installer les outils basiques (curl, vim, ...)
 - roles/apache/ -> Installer et démarrer le service apache2
 - roles/mysql/ -> Installer et démarrer le service mariadb
 - roles/php/ -> Installer les paquets php
 - roles/wordpress/ -> Télécharger WordPress

## Arborescence du projet
```
.
├── README.md
├── roles
│   ├── apache
│   │   ├── defaults
│   │   │   └── main.yml
│   │   ├── handlers
│   │   │   └── main.yml
│   │   ├── meta
│   │   │   └── main.yml
│   │   ├── README.md
│   │   ├── tasks
│   │   │   └── main.yml
│   │   ├── tests
│   │   │   ├── inventory
│   │   │   └── test.yml
│   │   └── vars
│   │       └── main.yml
│   ├── common
│   │   ├── defaults
│   │   │   └── main.yml
│   │   ├── handlers
│   │   │   └── main.yml
│   │   ├── meta
│   │   │   └── main.yml
│   │   ├── README.md
│   │   ├── tasks
│   │   │   └── main.yml
│   │   ├── tests
│   │   │   ├── inventory
│   │   │   └── test.yml
│   │   └── vars
│   │       └── main.yml
│   ├── mysql
│   │   ├── defaults
│   │   │   └── main.yml
│   │   ├── handlers
│   │   │   └── main.yml
│   │   ├── meta
│   │   │   └── main.yml
│   │   ├── README.md
│   │   ├── tasks
│   │   │   └── main.yml
│   │   ├── tests
│   │   │   ├── inventory
│   │   │   └── test.yml
│   │   └── vars
│   │       └── main.yml
│   ├── php
│   │   ├── defaults
│   │   │   └── main.yml
│   │   ├── handlers
│   │   │   └── main.yml
│   │   ├── meta
│   │   │   └── main.yml
│   │   ├── README.md
│   │   ├── tasks
│   │   │   └── main.yml
│   │   ├── tests
│   │   │   ├── inventory
│   │   │   └── test.yml
│   │   └── vars
│   │       └── main.yml
│   └── wordpress
│       ├── defaults
│       │   └── main.yml
│       ├── files
│       │   └── wordpress.conf
│       ├── handlers
│       │   └── main.yml
│       ├── meta
│       │   └── main.yml
│       ├── README.md
│       ├── tasks
│       │   └── main.yml
│       ├── tests
│       │   ├── inventory
│       │   └── test.yml
│       └── vars
│           └── main.yml
└── wordpress.yml
```
