#all: ubuntu_16 debian_10

#Executer l'ensemble des instructions ubunutu 16
ubuntu_16: apply_ubuntu_16.04

#Déployer une instance de l'image ubuntu 16
apply_ubuntu_16.04: img_ubuntu_16.04
	cd terraform && terraform apply -auto-approve

#Créer mon image packer ubuntu 16
img_ubuntu_16.04:
	packer build -only 'ubuntu_16.*' azure-ubuntu.pkr.json.pkr.hcl

#Executer l'ensemble des instructions debian 10
debian_10 : apply_debian_10

#Déployer une instance de l'image debian 10 
apply_debian_10: img_debian_10
	cd terraform && terraform apply -auto-approve

#Créer image debian 10
img_debian_10:
	packer build -only 'debian_10.*' azure-ubuntu.pkr.json.pkr.hcl

#Détruire les instances
destroy: 
	cd terraform && terraform destroy -auto-approve
