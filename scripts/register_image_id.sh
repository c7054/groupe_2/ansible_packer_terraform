#!/bin/bash

AMI_ID=$(cd .. | jq -r '.builds[-1].artifact_id' manifest.json | cut -d ":" -f2)
echo $AMI_ID
