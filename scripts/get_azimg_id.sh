#!/bin/bash
rm -f terraform/azimagevar.tf
IMG_ID=`jq -r '.builds[-1].artifact_id' manifest.json | cut -d ":" -f2`
cd terraform
echo 'variable "azimg_id" { default = "'${IMG_ID}'" }' > azimgvar.tf
