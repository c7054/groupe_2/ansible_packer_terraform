#Premiere source : Ubuntu 16
source "azure-arm" "ubuntu_16_04" {
  azure_tags = {
    dept = "DevOps"
    task = "Ubuntu 16.04 WordPress Image"
  }
  image_offer                       = "UbuntuServer"
  image_publisher                   = "Canonical"
  image_sku                         = "16.04-LTS"
  location                          = "East US"
  managed_image_name                = "Ubuntu_16.04_WordPress"
  managed_image_resource_group_name = "myResourceGroup"
  os_type                           = "Linux"
  subscription_id                   = "298ede54-46be-432e-8e63-31b0f5969505"
  tenant_id                         = "190ce420-b157-44ae-bc2f-69563baa5a3b"
  use_azure_cli_auth                = true
  vm_size                           = "Standard_B1s"
}

#Deuxieme source : Debian 10
source "azure-arm" "debian_10" {
  azure_tags = {
    dept = "DevOps"
    task = "Debian 10 WordPress Image"
  }
  image_offer			    = "Debian-10"
  image_publisher		    = "Debian"
  image_sku			    = "10"
  location                          = "East US"
  managed_image_name                = "Debian_10_WordPress"
  managed_image_resource_group_name = "myResourceGroup"
  os_type                           = "Linux"
  subscription_id                   = "298ede54-46be-432e-8e63-31b0f5969505"
  tenant_id                         = "190ce420-b157-44ae-bc2f-69563baa5a3b"
  use_azure_cli_auth                = true
  vm_size                           = "Standard_B1s"
}

#Instructions build Ubuntu 16
build {
  name = "ubuntu_16.04"

  sources = ["source.azure-arm.ubuntu_16_04"]

  provisioner "shell" {
    script = "./scripts/deploy_ansible_ubuntu.sh"
  }

  provisioner "ansible-local" {
    playbook_file = "./ansible/wordpress.yml"
    role_paths = [
      "ansible/roles/common",
      "ansible/roles/apache",
      "ansible/roles/php",
      "ansible/roles/mysql",
      "ansible/roles/wordpress"
    ]
  }

  post-processor "manifest" {
        output = "manifest.json"
        strip_path = true
  }

  post-processor "shell-local" {
  	script = "scripts/get_azimg_id.sh"
  }
}

#Instructions build Debian 10
build {
  name = "debian_10"

  sources = ["source.azure-arm.debian_10"]

  provisioner "shell" {
    script = "./scripts/deploy_ansible_debian.sh"
  }

  provisioner "ansible-local" {
    playbook_file = "./ansible/wordpress.yml"
    role_paths = [
      "ansible/roles/common",
      "ansible/roles/apache",
      "ansible/roles/php",
      "ansible/roles/mysql",
      "ansible/roles/wordpress"
    ]
  }

  post-processor "manifest" {
        output = "manifest.json"
        strip_path = true
  }

  post-processor "shell-local" {
  	script = "scripts/get_azimg_id.sh"
  }
}

